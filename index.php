<!DOCTYPE html>
<html>
<head>
  <title>Excel to JSON</title>

<script src="jquery.min.js"></script>
<script src="jszip.js"></script>
<script src="xlsx.js"></script>

<script>
    var ExcelToJSON = function() {

      this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
          var data = e.target.result;
          var workbook = XLSX.read(data, {
            type: 'binary'
          });
          workbook.SheetNames.forEach(function(sheetName) {
            // Here is your object
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            var json_object = JSON.stringify(XL_row_object);
            var parsed_json = JSON.parse(json_object);
            console.log(parsed_json);
            // console.log(parsed_json[1]["Batch Code"]);
            jQuery( '#xlx_json' ).val( json_object );
          })
        };

        reader.onerror = function(ex) {
          console.log(ex);
        };

        reader.readAsBinaryString(file);
      };
  };

  function handleFileSelect(evt) {
    
    var files = evt.target.files; // FileList object
    var xl2json = new ExcelToJSON();
    xl2json.parseExcel(files[0]);
  }


 
</script>

</head>
<body>



<form enctype="multipart/form-data">
    <input id="upload" type=file  name="files[]">
</form>

    <textarea class="form-control" rows=35 cols=120 id="xlx_json"></textarea>

    <script>

        document.getElementById('upload').addEventListener('change', handleFileSelect, false);

    </script>

</body>
</html>